# 一、简答题

## 1、请简述 Vue 首次渲染的过程。

1. 首先初始化Vue的实例成员跟静态成员
2. 新建Vue实例，调用构造函数，在构造函数调用this._init()，即Vue的入口
3. this._init()会调用entry-runtime-with-compiler.js中的$mount()方法，将模板编译成render函数。首先会判断是否在options中传入了render选项，如果没有，则获取template选项，template也没有的话，把el的内容作为模板，通过compilerToFunctions()去编译成render函数，编译完毕后，把render函数存放到options.render
4. 接下来用 runtime/index.js中的$mount，重新获取el并调用 mountComponent()方法。
5. 接下来调用mountComponent(),这个方法在src/core/instance/lifecycle.js中定义的，在mountComponent()中，首先会判断render选项，如果没有render选项，但是我们传入了模板，并且当前是开发环境的话会发送一个警告，目的是如果我们当前使用运行时版本的Vue,而且我们没有传入render,但是传入了模版,告诉我们运行时版本不支持编译器。接下来会触发beforeMount这个生命周期中的钩子函数，也就是开始挂载之前。
6. 然后定义了updateComponent()，在这个函数中，调用vm._render和vm._update，vm._render的作用是生成虚拟DOM，vm._update的作用是将虚拟DOM转换成真实DOM，并且挂载到页面上
7. 创建Watcher对象，在创建Watcher时，传递了updateComponent这个函数，这个函数最终是在Watcher内部调用的。在Watcher内部会用了get方法，当Watcher创建完成之后,会触发生命周期中的mounted钩子函数,在get方法中，会调用updateComponent()
8. 触发生命周期钩子mounted，返回Vue实例

## 2、请简述 Vue 响应式原理。

1. 为属性定义setter跟getter，每个vue实例都有一个watch对象，触发getter时，当前属性会有一个dep实例收集当前vue实例的watcher对象，触发setter时会遍历dep中收集到的watcher实例，执行watcher的update方法执行更新操作
2. 当前数据为数组时，修改默认方法，当执行完数组方法后会执行dep.notify
3. 当前数据为对象时，调用walk方法，遍历对象的每个属性，调用defineReactive方法
4. 如果watcher未被处理，会被添加到queue队列中，并调用flushSchedulerQueue()方法，该方法会触发对应的钩子函数以及调用watcher.run()更新视图

## 3、请简述虚拟 DOM 中 Key 的作用和好处。

作用：

标识节点在当前层级的唯一性，在进行比较的时候，会基于 key 的变化重新排列元素顺序。从而重用和重新排序现有元素

好处：

在执行updateChildren对比新旧Vnode的子节点差异时，通过设置key可以进行更高效的比较，便于复用节点。降低创建销毁节点成本，从而减少dom操作，提升更新dom的性能

## 4、请简述 Vue 中模板编译的过程。

1. compileToFunctions(template, ...)：模板编译的入口函数

- 先从缓存中加载编译好的 render 函数；
- 如果缓冲中没有，则调用 compile(template, options) 开始编译；

2. compile(template, options)：

- 先合并选项 options；
- 再调用 baseCompile(template.trim(), finalOptions) ；compile 的核心是合并 options ，真正处理模板是在 baseCompile 中完成的；

3. baseCompile(template.trim(), finalOptions)：

- 先调用 parse() 把 template 转换成 AST tree ；
- 调用 optimize() 优化 AST ，先标记 AST tree 中的静态子树，检测到静态子树，设置为静态，不需要在每次重新渲染的时候重新生成节点，patch 阶段跳过静态子树；
- 调用 generate() 将 AST tree 生成js代码；

4. compileToFunctions(template, ...)：

- 继续把上一步中生成的字符串形式的js代码转换为函数；
- 调用 createFunction() 通过 new Function(code) 将字符串转换成函数；
- render 和 staticRenderFns 初始化完毕，挂载到 Vue 实例的 options 对应的属性中。
# 1. Vue 3.0 性能提升主要是通过哪几方面体现的？

1. 响应式系统升级，proxy代替object.defineProperty
2. 编译优化, vue2 通过标记静态根节点，优化diff过程，vue3 标记和提升所有的静态根节点，diff只需要对比动态系节点内容
3. 源码体积优化，移除不常用的api；tree-shaking机制：依赖es6的模块化的语法，将无用的代码进行剔除



# 2.Vue 3.0 所采用的 Composition Api 与 Vue 2.x使用的Options Api 有什么区别？

options api 用包含一个描述组件选项的对象来定义一个组件，长期迭代的项目，组件会变得很复杂，不利维护；难以提取可重用的逻辑。composition api 将相同功能的代码放在一起，提高可读性和可维护性

# 3. Proxy 相对于 Object.defineProperty 有哪些优点？

definePropety需要在**初始化**的时候遍历所有成员，把对象属性转化为getter setter

Proxy的性能比defineProperty好，可以监听动态新增的属性，监听删除的属性，监听数组的索引和length属性

# 4. Vue 3.0 在编译方面有哪些优化？

- Vue.js 2.x中通过标记静态根节点，优化diff 的过程
- Vue.js 3.0中标记和提升所有的静态根节点，diff 的时候只需要对比动态节点内容
  - Fragments(升级vetur插件)
  - 静态提升
  - Patch flag
  - 缓存事件处理函数 **源码体积的优化**
- Vue.js 3.0中移除了一些不常用的 API
- Tree-shaking

# 5.Vue.js 3.0 响应式系统的实现原理？

- Proxy对象实现属性监听
- 默认监听动态添加的属性
- 默认监听属性的删除操作
- 默认监听数组索引和 length属性
- 可以作为单独的模块使用
- 多层属性嵌套，在访问属性过程中处理下一级属性
- reactive：
- 接收一个参数，判断这参数是否是对象。不是对象则直接返回这个参数，不做响应式处理
- effect：接收一个函数作为参数。作用是：访问响应式对象属性时去收集依赖
- track：
  - 接收两个参数：target 和 key
  - 如果没有 activeEffect，则说明没有创建 effect 依赖
  - 如果有 activeEffect，则去判断 WeakMap 集合中是否有 target 属性，
  - WeakMap 集合中没有 target 属性，则 set(target, (depsMap = new Map()))
  - WeakMap 集合中有 target 属性，则判断 target 属性的 map 值的 depsMap 中是否有 key 属性
  - depsMap 中没有 key 属性，则 set(key, (dep = new Set()))
  - depsMap 中有 key 属性，则添加这个 activeEffect
- trigger：
  - 判断 WeakMap 中是否有 target 属性
  - WeakMap 中没有 target 属性，则没有 target 相应的依赖
  - WeakMap 中有 target 属性，则判断 target 属性的 map 值中是否有 key 属性，有的话循环触发收集的 effect()


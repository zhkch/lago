# 工程化

## 解决的问题

- 传统语言或语法的弊端
- 无法使用模块化/组件化
- 重复的机械式工作
- 代码风格统一，质量保证
- 以来后端服务接口
- 整体依赖后端项目

> 一切以提高效率，降低成本，质量保证为目的的手段都属于工程化

# 脚手架工具

本质作用：创建项目基础结构，提供项目规范和约定

## yeoman基础使用

- 全局安装yo:

  `npm install yo -g   /  yarn global add yo`

- 安装generator

  `npm install generator-node -g / yarn gloabal add generator-node`

- 通过yo运行generator

  `yo node`



## 创建Generator模块

1. 创建一个名为**generator-XX**的目录，作为一个node module

2. yarn/npm init初始化

3. yarn add yeoman-generator 安装generaotor基类

4. 按照目录编写模块

    ![自定义generator目录结构](generator-category.png)

5. yarn link 将模块链接到全局范围
6. yo XX

### 根据模板创建文件

在生成器目录下添加模板文件夹， app/templates

```js
<%= title%> // 支持ejs语法
```

### 通过app/index.js writting 方法渲染模板

```js
writting () {
   //通过模板方式写入文件到目标目录

   //模板文件路径
   const tmpl = this.templatePath('foo.txt')//自动输出templates下的文件路径
   //输出目标路径
   const output = this.destinationPath('foo.txt')
   //模板数据上下文
   const context = { title: 'Hello lll', success: true}

   this.fs.copyTpl(tmpl, output, context)
 }
```

### 接收用户输入

```js
prompting () {
 // Yeoman 在询问用户环节会自动调用此方法
 // 在此方法中可以调用父类的 prompt() 方法发出对用户的命令询问
 return this.prompt([
   {
     type: 'input',
     name: 'name',
     message: 'Your project name',
     default: this.appname //appname 为项目生成目录名称
   }
 ])
 .then(answers => {
   // answer => { name: 'user input value' }
   this.answers = answers
 })
}

 writting () {
   //通过模板方式写入文件到目标目录

   //模板文件路径
   const tmpl = this.templatePath('bar.html')//自动输出templates下的文件路径
   //输出目标路径
   const output = this.destinationPath('bar.html')
   //模板数据上下文
   const context = this.answers

   this.fs.copyTpl(tmpl, output, context)
 }
```

## Vue Generator

```js
writting () {
 //把文件都通过模板转换到目标路径
 const templates = [
   'public/favicon.ico',
   'public/index.html',
   'src/assets/logo.png',
   'src/components/HelloWorld.vue',
   'src/App.vue',
   'src/main.js',
   '.gitignore',
   'babel.config.js',
   'package.json',
   'README.md',
 ]
 templates.forEach(template => {
   this.fs.copyTpl(
     this.templatePath(template),
     this.destinationPath(template),
     this.answers
   )
 })
}
```

### 执行yo 模块可能出现的错误

1. Trying to copy from a source that does not exist => 模板路径不正确,或不存在。
2. Unexpected token '=' in => EJS语法中<%= 百分号和等号之间不能有空格
3. ReferenceError: BASE_URL is not defined => BASE_URL 不是我们定义的 可以使用<%%= 转义EJS语法，保留员模板文件中的内容

### 发布generator

1. echo node_modules > .gitignore ,可能无法或略 node_modules文件夹，可以尝试在vscode中手动创建.gitignore文件，写入需要忽略的内容
2. git init
3. git add .
4. git comite -m 'xxxxx' (需要有注册过user.emaile或user.name)
5. git remote add origin '仓库地址'
6. git push -u origin master (注意可能会需要完成登录或授权流程)
7. yarn publish
8. version 回车
9. npm username 输入npm的username
10. npm email 输入npm的email
11. npm password 输入npm的password
12. 完成 （如果仓库地址报错，查看当前npm 源，修改yarn publish --registry=[https://register.yarnpkg.com,在执行一遍发布）

## plop

小型脚手架工具，一般时集成到其他项目，用来创建同类型的项目文件

1. 安装plop

2. 项目根目录创建plop-templates模板文件目录，[语法]( [https://handlebarsjs.com/](https://handlebarsjs.com/%EF%BC%89))

   ```js
   import Recat form 'react'
   export default () => {
    <div classname="{{name}}">
      <h1>{{name}}</h1>
    </div>
   }
   ```

3. 新建plopfile.js

```js
// Plop 入口文件，需要导出一个函数
// 此函数接收一个 plop 对象，用于创建生成器任务

module.exports = plop => {
  plop.setGenerator('component', {
    description: 'create a component',
    // 命令行交互
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'component name',
        default: 'MyComponent'
      }
    ],
    actions: [
      {
        type: 'add', // 代表添加文件
        path: 'src/components/{{name}}/{{name}}.js',
        templateFile: 'plop-templates/component.hbs'
      },
      {
        type: 'add', // 代表添加文件
        path: 'src/components/{{name}}/{{name}}.css',
        templateFile: 'plop-templates/component.css.hbs'
      },
      {
        type: 'add', // 代表添加文件
        path: 'src/components/{{name}}/{{name}}.test.js',
        templateFile: 'plop-templates/component.test.hbs'
      }
    ]
  })
}
```

4. 上面代码创建了名为component的生成器，> yarn plop component

# 自动化构建工具

grunt 生态好，基于临时文件去实现，构建速度相对较慢。

gulp 同时执行多个任务，速度快，生态完善。

fis 大而全，没有前两者的灵活

## grunt

1. yarn add grunt

2. 创建gruntfile.js

   ```js
   module.export = grunt => {
       grunt.registerTask(TASKNAME: string, DESCRIPTION?: string ,()=> {
           // task content
       })
       // 异步任务
       grunt.registerTask('async-task', function() {
           const done = this.async()
           setTimeout(() => {
               console.log('async task')
               done()
           }, 10)
       })
       grunt.registerTask('default', [...TASKNAME])
   }
   ```

3. yarn grunt TASKNAME 不加任务名则默认执行default任务

### 标记任务失败

```js
module.export = grunt => {
    grunt.registerTask('bad', () => {
        //
        return false
    })
    grunt.registerTask('bad-async', function() {
        const done = this.async()
        setTimeout(() => {
            //
            done(false)
        }, 10)
    })
}
```

### 多目标任务

```js

module.exports = grunt => {
    grunt.initConfig({
        build: {
            options: {
                test: 'test'
            },
            css: 'css',
            js: 'js'
        }
    })
    grunt.regiesterMultiTask('build', function() {
        console.log(this.options())
        console.log(`target: ${this.target},data: ${this.data}`)
    })
}
//yarn grunt build
```

### 插件使用

```js
module.exports = grunt => {
    grunt.initConfig({
        clean: {
            temp: 'temp/app.js'
        }
    })
    grunt.loadNpmTasks('grunt-contrib-clean')
}
```
一次性导入所有grunt插件
```js
const loadGruntTasks = require('load-grunt-tasks')
module.exports = grunt => {
  loadGruntTasks(grunt) // 加载所有插件
  grunt.registerTask('default', function(){
    console.log(options);
  })
}
```
## Gulp

取消了同步模式，需要手动调用done；4.0之前需要通过gulp.task注册任务；gulp基于流，读取转换和写入

```js
// gulpfile.js
exports.task = done => {
    //
    done()
}
exports.default = done => {
    //...
}
// yarn gulp task/yarn gulp
```

### 组合任务

```js
const { series, parallel } = require('gulp')
//const task1 = ...
// const task2 = ...
exports.foo = series(task1,task2,...)
exports.bar = parallel(task1,...)
```

### 构建过程工作原理

```js
const fs = require('fs')
const { Transform } = require('stream')

exports.default = () => {
    const read = fs.createReadStream('normalize.css')
    const write = fs.createWriteStream('normalize.css')
    // 文件转换流
    const transform = new Transform({
        transform: (chunk, encoding, callback) => {
            // chunk 读取流中的内容
            const input = chunk.toString()
            const output = input.replace(/\s+/g, '').replace(/\/\*.+？\*\//g,'')
            callback(null, output) // 第一个参数为错误对象
        }
    })
    read.pipe(transform)pipe(write)
    return read
}
```

### 文件操作API

```js
const {src, dest} = require('gulp')
const cleanCss = require('gulp-clean-css')
const rename = require('gulp-clean-rename')

export.default = () => {
    return src('src/*.css')
           .pipe(cleanCss())
           .pipe(rename({extname: '.min.css'}))
    		.pipe(dest('dist'))
}
```

### useref

使用useref有个注意点，html里面有构建注释时，才能正常运行。

```html
<!-- build:css assets/styles/vendor.css -->
  <link rel="stylesheet"href="/node_modules/bootstrap/dist/css/bootstrap.css">
<!-- endbuild -->
```



# 封装工作流

创建一个通用模块，定义好gulpfile，因为要发布这个模块，所以package.json对gulp的依赖是在dependencies，而不是devDependencies；main配置项指定此gulpfile。

在本地开发可以先yarn link 将通用模块link到全局

然后在其他其他模块就可以link 此模块名

通过process.cwd()，通用模块可以获取当前命令行所在的目录，然后去读取项目目录下的配置文件

其他模块引用通用模块时，直接调用通用模块的cli，不必再安装多一次gulp。通用模块的bin目录加入以下文件:

```js
#!/usr/bin/env node

process.argv.push('--cwd')
process.argv.push(process.cwd()) // 获取当前命令行所在目录
process.argv.push('--gulpfile')
process.argv.push(require.resolve('..')) // 获取gulpfile的绝对目录

require('gulp/bin/gulp')
```


#!/usr/bin/env node
// node cli 必须写文件头，linx或ma'cOS要把文件权限置为 755


const inquirer = require('inquirer') // 命令行交互询问工具
const path = require('path')
const fs = require('fs')
const ejs = require('ejs')

// 发起命令询问
inquirer.prompt([
    {
        type: 'input', // 问题的输入方法
        name: 'name', // 问题返回值的键
        message: "What's your project name?" // 屏幕提示
    }
])
.then(answer => {
    console.log(answer)
    // 根据用户输入生出文件

    // 模板目录
    const tmpDir = path.join(__dirname, 'templates')
    // 目标目录：即运行cli的目录
    const destDir = process.cwd()
     
    // 读取模板文件，转换到目标目录
    fs.readdir(tmpDir, (err, files) => {
        if(err) throw err;
        files.forEach(file => {
            // console.log(file)
            // 通过模板引擎渲染文件
            ejs.renderFile(path.join(tmpDir, file), answer, (err, result) => {
                if (err) throw err;

                // 将结果写入目标文件
                fs.writeFileSync(path.join(destDir, file), result)
            })
        })
    })
})
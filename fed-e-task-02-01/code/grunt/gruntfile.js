// 实现这个项目的构建任务

const sass = require('sass')
// 自动加载所有的grunt插件
const loadGruntTasks = require('load-grunt-tasks')

const data = {
  menus: [
    {
      name: 'Home',
      icon: 'aperture',
      link: 'index.html'
    },
    {
      name: 'Features',
      link: 'features.html'
    },
    {
      name: 'About',
      link: 'about.html'
    },
    {
      name: 'Contact',
      link: '#',
      children: [
        {
          name: 'Twitter',
          link: 'https://twitter.com/w_zce'
        },
        {
          name: 'About',
          link: 'https://weibo.com/zceme'
        },
        {
          name: 'divider'
        },
        {
          name: 'About',
          link: 'https://github.com/zce'
        }
      ]
    }
  ],
  pkg: require('./package.json'),
  date: new Date()
}

/* 
获取命令行参数，如yarn serve --port=5210 --open=true
但用npm的传参需要加上--  npm run serve -- --port=5210 --open=true
*/
let options = require('yargs').argv;
console.log(options)
const open = options.open || false
const port = options.port || 2080
let prod = options.production || options.prod
const branch = options.branch || 'gh-pages'


module.exports = grunt => {
  loadGruntTasks(grunt)
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    // 样式检查
    stylelint: {
      options: {
        configFile: '.stylelintrc',
        syntax: 'scss'
      },
      all: ['src/**/*.scss']
    },
    // js代码检查
    eslint: {
      options: {
        configFile: '.eslintrc'
      },
      target: ['src/**/*.js']
    },
    // scss 代码转换
    sass: {
      dist: {
        files: {
          'tmp/assets/styles/main.css': 'src/assets/styles/main.scss'
        }
      },
      options: {
        implementation: sass,
        sourceMap: true
      }
    },
    // es代码转换
    babel: {
      options: {
        sourceMap: true,
        presets: ['@babel/preset-env']
      },
      main: {
        files: {
          'tmp/assets/scripts/main.js': 'src/assets/scripts/main.js'
        }
      }
    },
    // html模板转换
    web_swig: {
      options: {
        swigOptions: {
          cache: false
        },
        getData: function(tpl) {
          return data
        }
      },
      dist: {
        files: {
          'tmp/index.html': 'src/index.html',
          'tmp/about.html': 'src/about.html'
        }
      }
    },
    watch: {
      js: {
        files: ['src/**/*.js'],
        tasks: ['babel']
      },
      css: {
        files: ['src/**/*.scss'],
        tasks: ['sass']
      },
      html: {
        files: ['src/**/*.html'],
        tasks: ['web_swig']
      }
    },
    // 服务器开启，由于yarn start需要在dist开启服务器，这里分别定义了两个target
    browserSync: {
      options: {
        notify: false,
        open: open,
        port: port,
        watchTask: true,
        server: {
          baseDir: ['tmp', 'src', 'public'],
          routes: {
            '/node_modules': 'node_modules'
          }
        }
      },
      serve: {
        src : ['tmp/**/*.css', 'tmp/**/*.html', 'tmp/**/*.js'],
        server: 'tmp'
      },
      prod: {
        options: {
          watchTask: false,
          server: {
            baseDir: ['dist']
          }
        }
      }
    },
    clean: ['tmp', 'dist'],
    useminPrepare: {
      html: 'index.html',
      options: {
        dest: 'dist'
      } 
    },
    // 合并html引入的css跟js
    concat: {
      generated: {
        files: [
          {
            dest: 'dist/assets/scripts/vendor.js',
            src: [
              "node_modules/jquery/dist/jquery.js",
              "node_modules/popper.js/dist/umd/popper.js",
              "node_modules/bootstrap/dist/js/bootstrap.js"
            ]
          },
          {
            dest: 'dist/assets/styles/vendor.css',
            src: [
              "node_modules/bootstrap/dist/css/bootstrap.css"
            ]
          }
        ]
      }
    },
    // useref替代插件
    usemin: {
      html: ['dist/index.html', 'dist/about.html']
    },
    // js压缩
    uglify: {
      'dist/assets/scripts/main.min.js': ['tmp/assets/scripts/*.js']
    },
    // 复制文件，由于yarn build需支持--prod/--production 这里添加多了一步未经压缩的js跟css复制操作
    copy: {
      html: {
        expand: true,
        flatten: true,
        src: 'tmp/*.html',
        dest: 'dist/'
      },
      source: {
        expand: true,
        src: 'public/**',
        dest: 'dist/'
      },
      css : {
        expand: true,
        flatten: true,
        src: 'tmp/assets/styles/*.css',
        dest: 'dist/assets/styles'
      },
      js: {
        expand: true,
        flatten: true,
        src: 'tmp/assets/scripts/*.js',
        dest: 'dist/assets/scripts/'
      }
    },
    cssmin: {
      'dist/assets/styles/main.min.css': ['tmp/assets/styles/*.css']
    },
    // 图片字体压缩
    imagemin: {
      dist: {
        options: {
          optimizationLevel: 1 //定义图片优化水平
        },
        files: [
          {
            expand: true,
            cwd: 'src/',//原图存放的文件夹
            src: ['**/*.{png,jpg,jpeg,gif,eot,svg,ttf,woff}'], // 优化 img 目录下所有 png/jpg/jpeg/gif图片
            dest: 'dist/' // 优化后的图片保存位置，覆盖旧图片，并且不作提示
          }
      ]
      }
    },
    // 代码发布
    git_deploy: {
      target: {
        options: {
          url: 'https://gitee.com/zhkch/lago.git',
          branch
        },
        src: 'dist'
      }
    }
  })

  grunt.registerTask('lint', ['eslint', 'stylelint'])
  grunt.registerTask('compile', ['sass', 'babel', 'web_swig'])
  grunt.registerTask('serve', ['browserSync:serve', 'watch'])
  grunt.registerTask('build', (function(){
    // 根据命令行参数决定是否压缩js css
    if (prod) {
      return ['clean', 'compile', 'copy:html', 'copy:source', 'useminPrepare', 'concat', 'uglify', 'cssmin', 'usemin', 'imagemin']
    } else {
      return ['clean', 'compile', 'copy', 'useminPrepare', 'concat','copy:css', 'copy:js', 'usemin', 'imagemin']
    }
  })() )
  grunt.registerTask('start', (function() {
    return ['clean', 'compile', 'copy:html', 'copy:source', 'useminPrepare', 'concat', 'uglify', 'cssmin', 'usemin', 'imagemin', 'browserSync:prod']
  })())
  grunt.registerTask('deploy', ['clean', 'compile', 'copy:html', 'copy:source', 'useminPrepare', 'concat', 'uglify', 'cssmin', 'usemin', 'imagemin', 'git_deploy'])
}
// 实现这个项目的构建任务

const { src, dest, parallel, series, watch } = require('gulp')
const loadPlugins = require('gulp-load-plugins')
const browserSync = require('browser-sync')
var minimist = require('minimist');
const plugins = loadPlugins()
const del = require('del')
const bs = browserSync.create()

/* 
获取命令行参数，如yarn serve --port 5210 --open true
但用npm的传参需要加上--  npm run serve -- --port 5210 --open true
*/
let options = require('yargs').argv;
const open = options.open || false
const port = options.port || 2080
let prod = options.production || options.prod
const branch = options.branch || 'gh-pages'


const data = {
  menus: [
    {
      name: 'Home',
      icon: 'aperture',
      link: 'index.html'
    },
    {
      name: 'Features',
      link: 'features.html'
    },
    {
      name: 'About',
      link: 'about.html'
    },
    {
      name: 'Contact',
      link: '#',
      children: [
        {
          name: 'Twitter',
          link: 'https://twitter.com/w_zce'
        },
        {
          name: 'About',
          link: 'https://weibo.com/zceme'
        },
        {
          name: 'divider'
        },
        {
          name: 'About',
          link: 'https://github.com/zce'
        }
      ]
    }
  ],
  pkg: require('./package.json'),
  date: new Date()
}


// 清除编译的文件
const clean = () => {
  return del(['dist', 'temp'])
}

// 执行eslint
const lintJS = () => {
  return src(['src/**/*.js'])
      // eslint()将lint输出附加到"eslint"属性，以供其他模块使用
      .pipe(plugins.eslint())
      // format()将lint结果输入到控制台
      .pipe(plugins.eslint.format())
      // 使进程退出时具有错误代码（1）
      // lint错误，最后将流和管道返回failAfterError。
      .pipe(plugins.eslint.failAfterError())
}

// 执行style lint
const lintStyle = () => {
  return src('src/**/*.scss')
      .pipe(plugins.sassLint({
        options: {
          formatter: 'stylish',
          'merge-default-rules': false
        }
      }))
      .pipe(plugins.sassLint.format())
      .pipe(plugins.sassLint.failOnError())
}
const lint = parallel(lintJS, lintStyle)

// 编译css
const css = () => {
  return src('src/assets/styles/*.scss', { base: 'src' })
    .pipe(plugins.sass({ outputStyle: 'expanded' }))
    .pipe(dest('temp'))
    .pipe(bs.reload({ stream: true }))
}

// 编译js
const js = () => {
  return src('src/assets/scripts/*.js', { base: 'src' })
    .pipe(plugins.babel())
    .pipe(dest('temp'))
    .pipe(bs.reload({ stream: true }))
}

// 压缩图片
const image = () => {
  return src('src/assets/images/**', { base: 'src' })
    .pipe(plugins.imagemin())
    .pipe(dest('dist'))
}

// 压缩字体
const font = () => {
  return src('src/assets/fonts/**', { base: 'src' })
    .pipe(plugins.imagemin())
    .pipe(dest('dist'))
}

// 静态资源
const source = () => {
  return src('public/**', { base: 'public' })
    .pipe(dest('dist'))
}

// 页面模板渲染
const html = () => {
  return src('src/*.html', { base: 'src' })
    .pipe(plugins.swig({ data, defaults: { cache: false } })) // 去除模板缓存
    .pipe(dest('temp'))
    .pipe(bs.reload({ stream: true }))
}

//启动开发服务器
const serve = () => {
  watch('src/assets/styles/*.scss', css)
  watch('src/assets/scripts/*.js', js)
  watch('src/*.html', html)

  watch([
    'src/assets/images/**',
    'src/assets/fonts/**',
    'public/**'
  ], bs.reload)

  bs.init({
    notify: false,
    open: open,
    port: port,
    server: {
      baseDir: ['temp', 'src', 'public'],
      routes: {
        '/node_modules': 'node_modules'
      }
    }
  })
}

// 启动生产模式服务器
const prodServe = () => {
  bs.init({
    notify: false,
    open: open,
    port: port,
    server: {
      baseDir: ['dist']
    }
  })
}

const useref = () => {
  console.log(`running build ${prod? 'with': 'without'} minify `)
  return src('temp/*.html', { base: 'temp' })
    .pipe(plugins.useref({ searchPath: ['temp', '.'] }))
    // html js css
    .pipe(plugins.if((prod)&&/\.js$/, plugins.uglify()))
    .pipe(plugins.if((prod)&&/\.css$/, plugins.cleanCss()))
    .pipe(plugins.if((prod)&&/\.html$/, plugins.htmlmin({
      collapseWhitespace: true,
      minifyCSS: true,
      minifyJS: true
    })))
    .pipe(dest('dist'))
}

const setProd = (done) => {
  prod = true
  done()
}

const upload = () => {
  return src('dist/**/*')
      .pipe(plugins.ghPages({
        branch
      }))
}

const compile = parallel(html, css, js)
const build = series(clean, series(compile, useref), image, font, source)
const dev = series(compile, serve)
const start =  series(setProd,build, prodServe)
const deploy = series(build, upload)

module.exports={
  clean,
  lint,
  compile,
  build,
  serve: dev,
  start,
  deploy
}
# 简答题

## 1、谈谈你对工程化的初步认识，结合你之前遇到过的问题说出三个以上工程化能够解决问题或者带来的价值

答：一切以提高效率，降低成本，质量保证为目的的手段都属于工程

1. 开发使用ES6+新特性，发布时让工具将新语法转换到低版本的兼容语法
2. 使用css一些预编译工具增强css的编程性
3. 模块化提高项目的可维护性
4. 部署上线前自动压缩代码及资源文件
5. 方便多人协作，统一代码风格
6. 开发过程可以实现mock数据，不必等待后端接口

## 2、你认为脚手架除了为我们创建项目结构，还有什么更深的意义？

答： 减少搭建时间，提高开发效率；提供了项目规范和约定，一套配置可以生出多个项目，有利于维护。



# 编程题

## 1、概述脚手架实现的过程，并使用 NodeJS 完成一个自定义的小型脚手架工具

答： 实现流程：

1. 通过命令行询问用户问题
2. 根据用户的输入结果结合模板文件，生出目录结构

[自定义脚手架](<https://gitee.com/zhkch/lago/tree/master/fed-e-task-02-01/code/my-cli>)

## 2、尝试使用 Gulp 完成项目的自动化构建

[代码目录](<https://gitee.com/zhkch/lago/tree/master/fed-e-task-02-01/code/gulp>)

## 3、使用 Grunt 完成项目的自动化构建

[代码目录](<https://gitee.com/zhkch/lago/tree/master/fed-e-task-02-01/code/grunt>)

## 构建思路
根据项目所要提供的命令行，决定每个命令行所要实现的功能
### yarn lint/npm run lint
引入css/es对应的lint插件，定义好任务即可
### yarn compile/npm run compile
引入css/es/html渲染对应的插件，定义好任务
### yarn serve/npm run serve
在compile任务执行后的目录，运行dev server工具，同时配置好各种静态文件目录的映射，即可完成此任务。另外要对命令行参数获取后，用于dev server的配置
1. gul项目执行方法：yarn serve --port 5210 --open true / npm run npm run serve -- --port 5210 --open true 。其中port 参数跟open参数有默认值，可以不填
2. grunt的执行方法有点区别： yarn serve --port=5210 --open=true / npm run serve -- --port=5210 --open=true   参数名跟参数值必须连在一起
### yarn build/npm run build
执行compile后，再把其他资源文件一并复制到dist目录。然后根据命令行参数决定是否要对css js进行压缩。由于grunt的useref插件没有gulp的好用，因此在grunt中使用了usemin插件，需要手动去合并css/js的vendor.
### yarn start/npm run start
执行完build后，在build后的目录开启服务器即可，加上参数配置
### yarn deploy/npm run deploy
执行完build，将整个目录上传到git对应分支即可.grunt 目录执行deploy时，需手动先创建远程分支
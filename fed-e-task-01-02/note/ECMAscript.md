# 概述

- js是es的扩展语言，es只提供了最基本的语法
- 在web环境，js = es+web APIs(BOM + DOM)
- 在node环境，js = es + Node APIs(fs + net + ...)

# ES2015

- 解决原有语法的问题或不足
- 对原有语法的增强
- 全新的对象，方法 功能
- 全新的数据类型跟结构

## 作用域

- 全局作用域
- 函数作用域
- 块级作用域（es6新增）

最佳实践：不用var 主用const 配合 let

## 解构

```js
const arr = [1,2,3]
const [a] = arr
const [a,...b] = arr
const [a = 0,b=3] = arr

const obj = {
    name: 'test',
    age: 12,
    address: 'china'
}
const {name: newName, age} = obj
const {name = 'jack'} = obj
const {name, ...etc} = obj
```

## 模板字符串

支持换行及插值表达式

**带标签的模板字符串**：

```js
const name = 'jack'
const gender = 'male'
let myTagFunc = (strings, name, gender) => {
    console.log(strings, name, gender)
}
const result = myTagFunc`hi,${name}is a${male}`
```



## 字符串扩展方法

- includes
- startsWith
- endsWith

## 对象字面量的增强

```js

const b = 2
const c = 'name'
let a = {
    b,
    print(){console.log('hello')},
    [c]: 'jack'
}


```

## 对象扩展

### Object.assign

将多个源对象中的属性复制到一个目标对象中，会覆盖目标对象的同名属性

Object.assign(target,source1,source2...)

### Object.is

判断两个值是否相等 NaN !== NaN    Object.is(NaN, NaN) == true  Object.is(+0, -0) == flase

### proxy

```js
const person = {name: 3, age: 3}
const objProxy = new P（roxy(person, {
    get(target, property) {
        console.log(target, property)
    },
    set(target, property, value) {
    	console.log(target, property, value)
	}
})
```

1. proxy可以监测更多操作如

   - **get(target, propKey,receiver)**

     拦截某个属性的读取操作，参数依次为目标对象，属性名，proxy实例本身（可选参数)

   - **set(target, propKey, value，receiver)**

     拦截某个属性的赋值操作，可以接受四个参数，依次为目标对象、属性名、属性值和 Proxy 实例本身，其中最后一个参数可选

   - **has(target, propKey)**

     拦截`HasProperty`操作，即判断对象是否具有某个属性时，这个方法会生效。典型的操作就是`in`运算符

     接受两个参数，分别是目标对象、需查询的属性名

   - **deleteProperty(target, propKey)**

     用于拦截`delete`操作，如果这个方法抛出错误或者返回`false`，当前属性就无法被`delete`命令删除

   - **ownKeys(target)**

     拦截对象自身属性的读取操作.具体来说，拦截以下操作

     - `Object.getOwnPropertyNames()`
     - `Object.getOwnPropertySymbols()`
     - `Object.keys()`
     - `for...in`循环

     使用`Object.keys()`方法时，有三类属性会被`ownKeys()`方法自动过滤，不会返回。

     - 目标对象上不存在的属性
     - 属性名为 Symbol 值
     - 不可遍历（`enumerable`）的属性

   - **getOwnPropertyDescriptior(target, propKey)**

     拦截`Object.getOwnPropertyDescriptor()`，返回一个属性描述对象或者`undefined`

   - **defineProperty(target, propKey, propDesc)**

     拦截了`Object.defineProperty()`操作

     ```js
     var handler = {
       defineProperty (target, key, descriptor) {
         return false;
       }
     };
     var target = {};
     var proxy = new Proxy(target, handler);
     proxy.foo = 'bar' // 不会生效
     ```

     上面代码中，`defineProperty()`方法内部没有任何操作，只返回`false`，导致添加新属性总是无效。注意，这里的`false`只是用来提示操作失败，本身并不能阻止添加新属性。

     注意，如果目标对象不可扩展（non-extensible），则`defineProperty()`不能增加目标对象上不存在的属性，否则会报错。另外，如果目标对象的某个属性不可写（writable）或不可配置（configurable），则`defineProperty()`方法不得改变这两个设置。

   - **preventExtensions(target)**

     `Object.preventExtensions()`。该方法必须返回一个布尔值，否则会被自动转为布尔值。

     这个方法有一个限制，只有目标对象不可扩展时（即`Object.isExtensible(proxy)`为`false`），`proxy.preventExtensions`才能返回`true`，否则会报错。

   - **getPrototypeOf(target)**

     拦截获取对象原型。具体来说，拦截下面这些操作

     - `Object.prototype.__proto__`
     - `Object.prototype.isPrototypeOf()`
     - `Object.getPrototypeOf()`
     - `Reflect.getPrototypeOf()`
     - `instanceof`

     注意，`getPrototypeOf()`方法的返回值必须是对象或者`null`，否则报错。另外，如果目标对象不可扩展（non-extensible）， `getPrototypeOf()`方法必须返回目标对象的原型对象。

   - **isExtensible(target)**

     拦截`Object.isExtensible()`操作。注意，该方法只能返回布尔值，否则返回值会被自动转为布尔值。返回值必须与目标对象的`isExtensible`属性保持一致，否则就会抛出错误。

   - **setPrototypeOf(target, proto)**

     用来拦截`Object.setPrototypeOf()`方法。

     注意，该方法只能返回布尔值，否则会被自动转为布尔值。另外，如果目标对象不可扩展（non-extensible），`setPrototypeOf()`方法不得改变目标对象的原型。

   - **apply(target, object,args)**

     拦截函数的调用、`call`和`apply`操作,三个参数，分别是目标对象、目标对象的上下文对象（`this`）和目标对象的参数数组

     ```js
     var twice = {
       apply (target, ctx, args) {
         return Reflect.apply(...arguments) * 2;
       }
     };
     function sum (left, right) {
       return left + right;
     };
     var proxy = new Proxy(sum, twice);
     proxy(1, 2) // 6
     proxy.call(null, 5, 6) // 22
     proxy.apply(null, [7, 8]) // 30
     ```

   - **construct(target, args)**

     用于拦截`new`命令，可以接收三个参数，target目标对象，args构造函数的参数对象，new命令作用的构造函数

     construct方法返回的必须是一个对象，否则报错

2. proxy 是以非侵入式的方式监管了对象的读写

### Reflect

属于静态类，其成员方法就是Proxy处理对象的默认实现

```js
const obj = {
    name: 'jack',
    value: 4
}
const proxy = new Proxy(obj, {
    get(target, property) {
        // 处理逻辑
        return Reflect.get(target, property)
    }
})
console.log(proxy.name)
// jack
```



## Set/WeakSet

值的集合，且不会拥有重复项

方法：add/has/delete/clear/entries/keys/values/forEach

```js

 var s = new Set(['a','b','c']);
    s.entries();
    //结果：SetIterator {["a", "a"], ["b", "b"], ["c", "c"]}
```

可见，Set结构是键名和键值是同一个值。

**WeakSet**与Set类似，但成员必须是对象类型的值。

WeakSet结构也提供了add( ) 方法，delete( ) 方法，has( )方法给开发者使用，作用与用法跟Set结构完全一致。
WeakSet 结构不可遍历。因为它的成员都是对象的弱引用，随时被回收机制回收，成员消失。所以WeakSet 结构不会有keys( )，values( )，entries( )，forEach( )等方法和size属性。

```js
const foos = new WeakSet()
class Foo {
  constructor() {
    foos.add(this)
  }
  method () {
    if (!foos.has(this)) {
      throw new TypeError('Foo.prototype.method 只能在Foo的实例上调用！');
    }
  }
}
```

上面代码保证了`Foo`的实例方法，只能在`Foo`的实例上调用。

## Map/WeakMap

JavaScript 的对象（Object），本质上是键值对的集合（Hash 结构），但是传统上只能用字符串当作键

Map可以使用任意类型的值作为键

方法：

- set(KEY, VALUE) 需要特别注意的是，Map 的遍历顺序就是插入顺序
- get
- delete
- clear

**WeakMap**只接受对象作为键名（null除外），不接受其他类型的值作为键名。

**WeakMap**的键名所指向的对象，不计入垃圾回收机制。

**WeakMap**没有遍历操作（即没有`keys()`、`values()`和`entries()`方法），也没有`size`属性,无法清空，即不支持`clear`方法

```js
const wm = new WeakMap();

const element = document.getElementById('example');

wm.set(element, 'some information');
wm.get(element) // "some information"
```

## Symbol

一种新的数据基本类型，主要作用为对象添加独一无二的属性名
还可以模拟实现对象的私有成员
```js
const name = Symbol()
const person = {
  [name]: 'test',
  say() {
    console.log(this[name])
  }
}
```

`Symbol.for()`与`Symbol()`这两种写法，都会生成新的 Symbol。它们的区别是，前者会被登记在全局环境中供搜索，后者不会。`Symbol.for()`不会每次调用就返回一个新的 Symbol 类型的值，而是会先检查给定的`key`是否已经存在，如果不存在才会新建一个值

```js
const obj = {
    [Symbol.toStringTag]: 'test'
}
console.log(obj)
// [object test]
```

## 迭代接口

所有可以被for...of遍历的对象都拥有[Symbol.iterator]方法

```js
const obj = {
    // 可迭代接口 iterable
    [Symbol.iterator]: function() {
        // 迭代器接口： iterator
        return {
            next: function() {
                // iterationResult
                return {
                    value: 'test',
                    done: true
                }
            }
        }
    }
}
```

## 生成器

```js
function * test() {
    yield 1;
    yield 2;
}
const foo = test()
foo.next()
foo.next()
```
















# 强类型、弱类型
从语言层面，强类型不允许数据**隐式**类型转换，后者可以。

# 静态类型、动态类型
静态类型： 一个变量声明时其类型就是明确的，声明过后，类型不可再修改
动态类型： 在运行阶段才能明确变量类型，且运行阶段类型可以改变

# 强类型的优势
1. 错误更早暴露
2. 代码更加智能，编码更加准确
3. 重构更可靠
4. 减少类型判断

# Flow:类型检查器

```js
const obj: { [string]: string} = {}
obj.key = 'test'
```
函数类型限制
```js
function foo(callback: (string, number) -> volid ) {
  callback('name', 34)
}
```
特殊类型
```js
const type: 'success'|'warning'|'danger' = 'warning';
type StringOrNumber = string | number
const value: StringOrNumber = '123'

const a: ?number = undefined 
//等同
const a: number | null | void = undefined
```

mixed/any 相当于其他类型的联合类型 number|null|void|string|...
但any属于弱类型，使用mixed才是类型安全的

[类型手册](https://www.saltycrane.com/cheat-sheets/flow-type/latest/)

# TypeScript

## ts配置文件
> yarn tsc --init
生成配置文件
- target: 编译后的文件符合的标准
- module: 用何种方式去模块化
- lib: 标准库，内置对象对应的声明

## 类型
1. object
   ```ts
   const obj = {
     name: string, 
     age: number,
    }
   ```
2. 数组
   ```ts
   const arr: Array<number> = [1,2,3]
   const arr: number[] = [1,2,3]
   ```
3. 元组
   ```ts
   const arr: [number, string] = [1,'name']
   ```
4. 枚举
   ```ts
   enum Status {
     math = 1,
     music = 2
   }
   // const enum的话，不会生成键值对对象，不能通过枚举值获取枚举名称。如不需要枚举名称，则加const
   // 不指定值的话，则是从0开始自增长;只给第一个值赋值则从第一个值开始增长
   const class = {
     status: Status.math
   }
   ```
5. 函数类型
   ```ts
   function test(name: string, age?: number): string {
     return ''
   }
   ```
6. any

## 类型断言
```ts
const nums = [1,3,45]
const res = nums.find(i => i > 0)
const num = res as number
const num1 = <number>res //jsx 不能使用
```

## 接口: 类型约束
```ts
interface Person {
  name: string;
  readonly age: number; // 只读
  address?: string;
  [key: string]: any
}
```

## 类
```ts
class Person {
  name: string;
  constructor(name: string) {
    this.name = name
  }
  sing(song: string) {
    console.log(song)
  }
}
```
### 类访问修饰符
- public 可以通过实例属性访问
- protected 只能在子类中访问
- private 私有属性只能在类的内部访问

### 抽象类,只能被继承不能被实例化
```ts
abstract class Animal{
  eat(food: string): void{
    console.log(food)
  }
  abstract run(distance: number): void
}
class Dog extends Animal {
  run(distance: number):void {
    console.log(distance)
  }
}
```

## 泛型

定义时不能明确的类型变为一个参数，使用时传递。

```ts
function createArray<T>(length: number, value:T): T[]{
    const arr = Array<T>(length).fill(value)
    return arr
}
const res = createArray<string>(3, 'foo')
```

## 类型声明

```js
declare function test()：string
```







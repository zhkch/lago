# ES Module

在script中添加type=module属性，即可以用ESmodule 标准执行js代码

1. ESM自动采用严格模式

2. 每个ESM都是运行在单独的私有作用域

3. ESM通过CORS方式请求外部js模块
4. ESM script标签会延迟执行脚本，相当于自动加入defer

export的变量是值得引用，而不是值的拷贝，且只能在定义的模块里改变其值

## 导入用法

1. 导入的时候要写完整的文件名

2. 用相对路径导入时，不能省略  ./

3. 可以使用绝对路径或者url

4. 导入一个不需要对外暴露成员的模块： import './xx.js'

5. 导入模块所以成员 import * as obj from './module.js'

6. 动态导入模块

   ```js
   import('./module.js').then(function(module) => {
          console.log(module)
   })
   ```

node 执行ESM 需要加上--experimental-modules

## ESM 与 CommonJs交互

1. ESM 中可以导入CommonJS模块
2. CommonJS不能导入ESM
3. CommonJS始终只会导出一个默认对象
4. import不是解构导出对象

## ESM 在node中的差异

在CommonJS的模块中，有五个变量：require module exports `__filename` `__dirname`

ESM无法使用这些成员，可以通过以下代码获取filename 跟 dirname

```js
//import.meta.url 文件url
import { fileURLPToPath } from 'url'
import { dirname } from 'path'
const __filename = fileURLToPath(import.meta.url)
const __dirname = dirname(__filename)

```

# webpack 打包

解决已存在问题：

1. ESM 存在环境兼容问题
2. 模块文件多，网络请求频繁
3. 前端资源模块化

## 配置文件

```js
// webpack.config.js
const path = require('path')
module.exports = 
    mode: 'development', //'production', 'none', 或者命令行中加入 --mode参数
    entry: './src/main.js', // 入口文件， 相对路径，前面的./ 不能省略
    output: { // 配置文件名,默认为dist/main.js
    	filename: 'bundle.js',
    	path: path.join(__dirname, 'dist') // 绝对路径
	}
}
```

## webpack 资源模块加载

webpack默认处理js文件，处理css文件需要用css loader,将css添加到bundle中，再通过style-loader创建style标签加到页面

```js
const path = require('path')
module.exports = {
    mode: 
    entry: './src/main.css',
    output: {
    	filename: 'bundle.js',
        path: path.join(__dirname, 'dist')
	},
    module: {
        rules: [
            {
				test: /.css$/,
                use: ['style-loader','css-loader'] //从后往前执行
            }
        ]
     }
}
```

## webpack导入资源模块

entry: './src/main.js', 直接在js文件中 import css再打包，

思想：在需要用到资源的时候，动态的引入资源

导入文件： file-loader 

```js

const path = require('path')
module.exports = {
    mode: 
    entry: './src/main.js',
    output: {
    	filename: 'bundle.js',
        path: path.join(__dirname, 'dist'),
        publicPath: 'dist/' //最后斜线不能去掉；将资源文件打包放到dist目录，并将这个路径最为返回值返回，这样在引用文件的js中才能取到正确的文件路径，否则不加此选项，webpack将资源文件的路径默认为网站根目录
	},
    module: {
        rules: [
            {
				test: /.css$/,
                use: ['style-loader','css-loader'] //从后往前执行
            },{
                test: '/.png$/',
                use: ['file-loader']
            }
        ]
     }
}
```

## url 加载器

url-loader可以将图片文件等转换为base64，适合小文件，减少请求次数

```js
module.export = {
    ...,
    module: {
        rules: [
			test: /.png$/,
            use: {
				loader: 'url-loadr',
                options: {
					limit: 10*1024// 将10kb以下的图片转换为data-url
           		 }
        	}
        ]
	}
}
```

## webpack转换es6

安装@babel/core @babel/preset-env babel-loader

```js
module.export = {
    ...,
    module: {
        rules: [
            {
                test: /.js$/,
                use: 'babel-loader',
                options: {
                    presets: ['@babel/preset-env']
                }
            }
        ]
    }
}
```

## webpack加载资源的方式

- 支持ESM
- 支持CommonJS
- 支持AMD

如果用require导入一个ESM的默导出，则是 const a = require('./b.js').default

```js
module.export = {
    module: {
        rules: [
            {
                test: /.html$/,
                use: {
                    loader: 'html-loader', // 默认只处理image标签的src属性
                    options: {
                        attrs: ['img:src', "a:href"] //加入a标签的资源处理
                    }
                }
            }
        ]
    }
}
```

## loader原理

loader可以看作一个个管道，将输入，转换为js代码, 拼接到bundle.js中

[例子](../code/markdown)

## webpack插件

loader专注实现资源模块的加载，插件增强webpack在项目自动化的能力

### 自动清除输出目录插件

```js
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
module.exports = {
    ...,
    plugins: [
        new CleanWebpackPlugin()
    ] 
}
```

### 自动生成html插件

```js
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
module.exports = {
    ...,
    output: {
    	filename: 'bundle.js',
        path: path.join(__dirname, 'dist'),
        //publicPath: 'dist/' htmlwebpackplugin将输出后的index.html也放到dist目录，所以也不用指定此选项
	},
    plugins: [
        new CleanWebpackPlugin()
        // 生成单个index.html
        new HtmlWebpackPlugin({
        	title: 'test', // 页面title
            meta: {
				viewport: 'width=device-width'
            },
            template: './src/index.html' // 在index.html使用模板 <%= htmlWebpackPlugin.options.title %>
        })
        // 继续用实例创建多个页面的htmlwebpackplugin的实例
        new HtmlWebpackPlugin()
		...
    ] 
}
```

### 插件原理

plugin通过钩子机制实现，是一个函数或者一个包含apply方法的对象

### webpack-dev-server

安装后运行，自动读取webpack配置文件，打包后启动服务器

#### 配置webpack-dev-server静态资源访问

```js
// 磁盘读写慢，开发环境不用copy插件复制静态资源， webpack.config.js加入配置项
devServer: {
    contentBase: ['./public']
}
```

#### 设置代理API

```js
module.exports = {
    mode: 'none',
    devServer: {
        contentBase: './public',
        proxy: {
            '/api': {
                // 将http://localhost:8080/api/user 代理到 https://api.github.com/api/user
                target: 'https://api.github.com',
                pathRewirte: {
                    // 路径重写为https://api.github.com/user
                    '^/api': ''
                },
                // 不能使用localhost：8080作为请求github的主机名
                changeOrigin: true
            }
        }
    }
}
```

### sourceMap

```js
// 配置项
devtool: 'source-map'
```

![图示](D:\code\learn\class\lago-fed\fed-e-task-02-02\note\eval-mode-sourcemap.png)

- eval 是否使用eval执行模块代码
- cheap 是否包含行信息
- module 是否能够得到loader处理前的源代码
- inline 将sourcemap以dataurl方式嵌入到源代码
- nosources 可以看到错误出现位置，不能定位到源码

sourcemap选择

开发模式：cheap-module-eval-source-map

生产模式： 不用sourcemap

## HMR

```js
 
devServer: {
    hot: true
},
plugins: [
   new webpack.HotModuleReplacementPlugin()     
]
```

webpack中的HMR需要手动处理模块热替换逻辑

样式文件通过loader处理，自动处理了热更新

在代码中通过

```js
module.hot.accept('./xx', () => {
    // TODO
})
```

### HMR注意事项

1. 处理HMR的代码报错会导致自动更新，可将devServer的hot属性改为hotOnly：true
2. 没启用HMR的情况下，HMR API报错
3. 多了与业务无关的代码，但是打包结果并不会把HMR相关代码加进去

## webpack生产环境优化

### 1.配置文件根据环境不同导出不同配置

```js
// yarn webpack --env production
// webpack.config.js
module.exports = (env, argv) => {
    const config = {...}
     if (env === 'production') {
         config.mode = 'production'
         config.devtool = false
         ...
    }
}
```

### 2.不同环境对于不同配置文件

创建一个公共通用配置文件

创建dev跟prod文件

```js
// webpack.dev.js/webpack.prod.js
const common = require('./webpack.common')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const merge = require('webpack-merge')

module.exports = merge(common, {
    mode: 'production',
    plugins: [
        new CopyWebpackPlugin(['public'])
    ]
})
```

## webpack 优化

### DefinePlugin

webpack的内置模块，为代码注入全局成员

注入了process.env.NODE_ENV

```js
const webpack = require('webpack')
module.exports = {
    mode: 'none',
    entry: './src/main.js',
    output:{
        filename: 'bundle.js'
    },
    plugins: [
        new webpack.DefinePlugin({
            API_BASE_URL: '"https://xx.com"' // DefinePugin的值是一段js代码，也可用JSON.stringfy代替
        })
    ]
}

// main.js
console.log(API_BASE_URL)
```

### tree Shaking

去掉未引用代码，production模式自动启用

```js
// 其他模式开启tree shaking
optimization: {
    usedExports: true // 只导出外部使用了的成员
    minimize: true //去掉未引用代码
    concatenateModules: true //合并模块(scope hoisting) 尽可能将所有模块合并输出到一个函数中
}
```

### tree Shaking 与babel

tree Shaking的实现是以ESM的方式组织代码。

最新版本babel-loader关闭了ESM转换的插件，如需转换，参照以下配置

```js
module: {
    rules: [
        {
            test: /\.js$/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: [
                        ['@babel/preset-env', {modules: 'commonjs'}] //设为false则为ESM
                    ]
                }
            }
        }
    ]
}
```

### sideEffects

一般用于npm包标记是否有副作用

```js
// package.json
{
    "sideEffects"：false
}

// webpack.config.js
module.exports = {
    optimization: {
        sideEffects: true, //production模式自动开启 开启特性后，打包时检查当前代码所属package.json有没有sideEffcts标识以此判断此模块是否有副作用，如果没有，则不会导出没有使用过的模块代码
    }
}
```

#### 使用前提

代码里没有副作用。如果副作用，在package.json把sideEffects配置项填入相关的文件名

## 代码分割

### 多入口打包

适用于多页应用

```js
entry: {
    index: './src/index.js',
    album: './src/album.js'
},
output: {
    filename: '[name].bundle.js' //
},
plugins: [
    new HtmlWebpackPlugin({
        chunks: ['index']
    }),
    new HtmlWebpackPlugin({
        chunks: ['album']
    })  
]
```

### 提取公共模块

```js
optimization: {
    splitChunks: {
        chunks: 'all'  // 把公共模块提取到另外一个bundle中
    }
}
```

## 动态导入

动态导入的模块会自动分包，加入注释可以修改打包后的文件名

```js
import(/* webpackChunkName: 'post' */'./post/posts').then(({default: post}) => {
    
})
```

## MiniCssExtractPlugin

提取css到单个文件，css文件较大时，使用此方式有优势，否则直接写在style中，能减少一次请求

```js
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
module.exports= {
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    // 'style-loader' 通过style引入css
                    MiniCssExtractPlugin.loader, //通过link方法引入
                    'css-loader'
                ]
            }
        ],
        plugins: [
            new MiniCssExtractPlugin()
        ]
    }
}
```

## OptimizeCssAssetsWebpackPlugin

webpack对代码的压缩默认只针对js

```js
const OptimizaCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin')
const TerserWebpackPlugin =  require('terser-webpack-plugin')
module.exports = {
    ...,
    plugins: [
       // 在此处配置，无论何种工作模式启动该，都会运行，可以放到minimizer中
        new OptimizaCssAssetsWebpackPlugin()
    ],
    // 最佳实践：为以下用法
    optimization: {
        minimizer: [ // 配置此属性会覆盖原有配置，需手动加入js压缩
            new OptimizeCssAssetsWebpackPlugin()
            new TerserWebpackPlugin()
        ]      
    }
}
```

## 输出文件名hash

hash/chunkhash/contenthash

```js
output： {
    filename： '[name]-[hash].bundle.js'
}
```

# rollup

相比webpack更为小巧，自是一个ESM模块打包器，没有HMR等高级特性。默认只处理ESM

```js
// rollup.config.js
export default {
    input: 'src/index.js',
    output: {
        file: 'dist/bundle.js',
        format: 'iife' // 输出格式
    }
}
// yarn rollup --config rollup.config.js
```

## rollup的插件是唯一的扩展方式

```js
import json from 'rollup-plugin-json' // 使用json插件
import resolve from 'rollup-plugin-node-resolve' // 加载npm第三方模块插件
import commonjs  from 'rollup-plugin-commonjs' // rollup默认处理ESM，需用插件来使用commonjs模块
export default {
    input: 'src/index.js',
    output: {
        file: 'dist/bundle.js',
        format: 'iife'
    },
    plugins: {
        json()，
        resolve(),
    	commonjs()
    }
}
```

## 代码拆分

```js
// 代码里动态导入 index.js
import('./vendor').then(module => {
    module.method()
})

// rollup.config.js
export default {
    input: 'src/index.js',
    output: {
        dir: 'dist' // 拆分指定目录，不能指定file文件名
        format: 'amd' //umd iife不支持拆分
    }
}
```

# Parcel

零配置的前端打包器



# 规范化标准

多人协同，统一标准，减少维护成本(代码，文档，提交日志)

## ESLint

ESLint --init 生成配置文件

```js
module.exports = {
    env: {
        browser: true, // 代码执行环境，判断相关api是否可用，
        es2020: true // 
    },
    extends: [
        'standard' // 继承生成配置文件时，所选择的风格
    ],
    parseOptions: {
        ecmaVersion: 11 // 控制es 版本
    },
    rules: {
        
    }
}
```

### 配置注释

```js
// eslint-disable-line
```

### ESLint 结合自动化工具

```js
// 执行eslint
const lintJS = () => {
  return src(['src/**/*.js'])
      // eslint()将lint输出附加到"eslint"属性，以供其他模块使用
      .pipe(plugins.eslint())
      // format()将lint结果输入到控制台
      .pipe(plugins.eslint.format())
      // 使进程退出时具有错误代码（1）
      // lint错误，最后将流和管道返回failAfterError。
      .pipe(plugins.eslint.failAfterError())
}
```

### ESLint 结合webpack

```js
// options 1
rules: [
    {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ['babel-loader', 'eslint-loader']
    }
] 
// optinos 2 更优
rules: [
    {
        test: /\.js$/,
        exclude: /node_modules/,
        use: 'babel-loader'
    },
    {
        test: /\.js$/，
        exclude: /node_modules/,
        use: 'eslint-loader',
        enforce: 'pre' // 由于其他loader执行
    }
]
```

### ESLint React

```js
//.eslintrc.js
rules: {
    'react/jsx-uses-react': 2,
    'react/jsx-uses-vars': 2
},
plugins: [
    'react'
 ]

// 使用共享配置,无需上面的手动配置，只需继承共享配置
extends: [
    'standard',
    'plugin:react/recommended'
]
```

## style lint

```js
// .stylelintrc.js
module.exports = {
    extends: 'stylelint-config-standard' // 安装共享配置
}
```



## Prettier

自动格式化工具

--write 将输出覆盖原文件



## Git Hooks

```js
安装husky, 自动把任务添加到git hooks的pre-commit钩子
// package.json
"scripts": {
    "test": "eslint index.js"
}
"husky": {
    "hooks": {
        "pre-commit": "npm run test"
    }
}
```

### lint-staged 结合husky

```js
// package.json
"scripts": {
    "precommit": "lint-staged"
}
"husky": {
    "hooks": {
        "pre-commit": "npm run precommit"
    }
}
"lint-staged": {
    "*.js": [
        "eslint",
        "git add"
    ]
}
```


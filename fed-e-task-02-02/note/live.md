```js
/** @type {import('webpack').Configuration} */
module.exports = {
    mode: 'none',
    // target: 'node' // fs path内置模块不参与打包
    externals: {
        // fs: 'require("fs")'
        fs: 'commonjs2 fs'
    }
}
```

https://shimo.im/docs/HQVQdqP9Dp69Qhhg
const marked = require('marked')
module.exports = source => {
    const html = marked(source)
    // option 1 在此loader中处理 module.exports 可替换为export default
    return `module.exports = ${JSON.stringify(html)}`

    // option2  返回html字符串交给下一个loader处理
    // return html
}
const path = require('path')
module.exports = {
    mode: 'none',
    entry: './src/main.js',
    output: {
        filename: 'bundle.js',
        path: path.join(__dirname,'dist'),
        publicPath: 'dist/'
    },
    module: {
        rules: [
            // option 1直接在markdown-loader中处理
            {
                test: /.md$/,
                use: ['./markdown-loader']
            }
            // option 2交给其他loader处理
            // {
            //     test: /.md$/,
            //     use: [
            //         'html-loader',
            //         './marked-loader'
            //     ]
            // }
        ]
    }
}
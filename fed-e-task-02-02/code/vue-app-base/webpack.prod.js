const common = require('./webpack.common')
const { merge } = require('webpack-merge');
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin')
const webpack = require('webpack')


module.exports = merge(common, {
  mode: 'production',
  output: {
    filename: '[name]-[contenthash:8].bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.(c|le)ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'less-loader'
        ]
      },
      {
        test: /\.js$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                ['@babel/preset-env', {
                  modules: 'commonjs'
                }]
              ]
            }
          }
        ],
        exclude: file => (
          /node_modules/.test(file) && !/\.vue\.js/.test(file)
        )
      }
    ]
  },
  optimization: {
    // 移除模块副作用的代码
    sideEffects: true,
    // 模块只导出被使用的成员
    usedExports: true,
    // 尽可能合并每一个模块到一个函数中
    minimize: true,
    // 压缩输出结果
    concatenateModules: true,
    splitChunks: {
      chunks: 'all'
    }
  },
  plugins: [
    new CleanWebpackPlugin(),
    new CopyWebpackPlugin({
      patterns: ['public']
    }),
    new MiniCssExtractPlugin({
      filename: '[name]-[contenthash:8].bundle.css'
    }),
    new OptimizeCssAssetsWebpackPlugin(),
    new webpack.DefinePlugin({
        BASE_URL: '"/"'
    })
  ]
})
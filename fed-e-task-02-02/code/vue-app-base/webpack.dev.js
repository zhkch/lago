const common = require('./webpack.common')
const webpack = require('webpack')
const { merge } = require('webpack-merge');
const { resolve } =  require('path')

module.exports = merge(common, {
    mode: 'development',
    devServer: {
        hotOnly: true,
        contentBase: './public',
        publicPath: process.env.BASE_URL,
        open: true,
    },
    devtool: 'cheap-module-eval-source-map',
    module: {
        rules: [
            {
                test: /\.(js|vue)$/,
                exclude: resolve('node_modules'),
                enforce: "pre",
                use: [
                  {
                    loader: 'eslint-loader',
                    options: {
                      //eslint检查报告的格式规范
                      formatter: require("eslint-friendly-formatter")
                    }
                  }
                ]
            },
            {
                test: /\.js?$/,
                loader: 'babel-loader',
                exclude: file => (
                    /node_modules/.test(file) && !/\.vue\.js/.test(file)
                )
            },
            {
                test: /\.(c|le)ss$/,
                use: ['vue-style-loader', 'css-loader', 'less-loader']
            }
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.DefinePlugin({
            BASE_URL: '"/"'
        })
    ]
})
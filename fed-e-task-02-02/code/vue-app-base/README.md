# vue-app-base

1. 这是一个使用 Vue CLI 创建出来的 Vue 项目基础结构
2. 有所不同的是这里我移除掉了 vue-cli-service（包含 webpack 等工具的黑盒工具）
3. 这里的要求就是直接使用 webpack 以及你所了解的周边工具、Loader、Plugin 还原这个项目的打包任务
4. 尽可能的使用上所有你了解到的功能和特性


# 说明文档：
1. 分析公共配置，开发环境跟生产环境均需把vue文件编译解析，对图片资源进行加载处理，并把html模板编译
2. 开发环境开启sourceMap,dev-server,添加eslint，通过babel-loader编译ES6+，加载css
3. 生产环境拷贝静态资源，tree shaking掉无关代码，减少代码体积，压缩css文件
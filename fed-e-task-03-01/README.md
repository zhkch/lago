# 一，简答题

## 1. 当我们点击按钮的时候动态给data增加的成员是否是响应式数据，如果不是的话，如何把新增成员设置成响应式数据 ，它的内部原理的什么。

```js
let vm = new Vue({
 el: '#el'
 data: {
  o: 'object',
  dog: {}
 },
 method: {
  clickHandler () {
   // 该 name 属性是否是响应式的
   this.dog.name = 'Trump'
  }
 }
})
```

答：这种情况设置的成员不是响应式的，可以通过 Vue.set或者实例方法vm.$set，调用内部的Observer去设置对应的setter跟getter方法

## 2 请简述 Diff 算法的执行过程

diff通过同层的树节点进行比较

1. 老节点不存在，则添加新节点到老节点
2. 新节点不存在，则从父元素删除老节点
3. 新老节点都存在：
   1. 判断是否存在相同节点：
      - 相同直接返回
      - 不是相同节点：
        1. 如果新老节点是静态的，且key相同。从老节点拿过来，跳过对比的过程
        2. 如果新节点是文本节点，设置节点的text
        3. 新节点不是文本节点：
           - 新老节点子节点都存在且不同，使用updateChildren函数更新子节点
           - 只有新节点存在子节点，如果老节点子节点是文本节点删除老节点的文本，将新节点子节点插入
           - 只有老节点存在子节点，删除老节点的子节点
   2. updateChildren
      - 给新老节点定义开始/结束索引
      - 循环比对新节点开始VS老节点开始、新节点结束VS老节点结束、新节点开始VS老节点结束、新节点结束VS老节点开始并移动对应的索引，向中间靠拢
      - 根据新节点的key在老节点中查找，没有找到则创建新节点
      - 循环结束后，如果老节点有多的，则删除。如果新节点有多的，则添加

# 二 编程题

## 1.模拟VueRouter的hash模式的实现，实现思路和History模式类似，把URL中的#后面的内容作为路由的地址，可以通过hashchange事件监听路由地址的变化

hash模式与history模式的不同点是事件的处理

```js
clickHandle(e) {
    location.hash = this.to
    this.$router.data.current = this.to
    e.preventDefault()
}
initEvent() {
    window.addEventListener('hashchange',() => {
        this.data.current = location.hash.slice(1)
    })
}
```

## 2. 在模拟Vue.js响应式源码的基础上实现v-html指令，以及v-on指令

v-html实现，新增一个指令处理函数

```js
htmlUpdater(node, value, key) {
    node.innerHTML = value;
    new Watcher(this.vm, key, (new) => {
        node.innerHTML = newValue
    })
}
```

v-on 实现：

1. 处理属性节点时，将v-on的事件类型提取出来
2. 在node中监听事件
3. 在实例中调用对应的方法

```js
compileElement(node) {
        // 遍历所有属性节点
        Array.from(node.attributes).forEach(attr => {
            // 判断是否是指令
            let attrNamme = attr.name
            if (this.isDirective(attrNamme)) {
                attrNamme = attrNamme.substr(2)
                let key = attr.value
                //#region 处理v-on
                if (attrNamme.indexOf('on:') !== -1) {
                    let eventType = attrNamme.split(':')[1]
                    this.handleEvent(this, node, eventType, key)
                }
                //#endregion
                this.update(node, key, attrNamme)
            }
        })
    }
handleEvent(vm, node, eventType, eventName) {
        // TODO: 这里简单地直接调用实例方法，完整的处理需要考虑传参
        node.addEventListener(eventType, () => {
            vm.vm.$options.methods[eventName]()
        })
    }
```
### 3.参考 Snabbdom 提供的电影列表的示例，利用Snabbdom 实现类似的效果，
[作业链接](<https://gitee.com/zhkch/lago/tree/master/fed-e-task-03-01/code/snabbdom-movie>)

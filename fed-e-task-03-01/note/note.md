# Vue-Router原理实现

## 动态路由传参

```js
//router index.js
const routes = [
{
    name: 'detail',
    path: '/detail/:id',
    component: detail,
    	props: true
    }
]

// detail.vue
{{$route.params.id}}
// detail 组件中接收路由参数
const detail = {
props: ['id'],
template: '<div>Detail ID： {{ id }}</div>'
}
```

## 嵌套路由

```js
{
    path: '/',
    component: Layout,
    children: [
      {
        name: 'index',
        path: '',
        component: Index
      },
      {
        name: 'detail',
        path: 'detail/:id',
        props: true,
        component: () => import('@/views/Detail.vue')
      }
    ]
  }
// layout.vue
<template>
  <div>
    <div>
      <img width="25%" src="@/assets/logo.png">
    </div>
    <div>
      <router-view></router-view>
    </div>
    <div>
      Footer
    </div>
  </div>
</template>
```

## 编程式导航

```js
// 跳转到指定路径
router.push('/login')
// 命名的路由
router.push({ name: 'user', params: { id: '5' }})
router.replace()
router.go()
```

## Hash与History模式

- hash基于锚点，以及onhashchange事件
- history 模式基于HTML5中的History API
  - history.pushState()不会向服务器请求，只改变浏览器url，并记录历史 IE10后支持 history.push会向服务器请求
  - history.replaceState()

### History模式

- 需要服务器支持-
- 单页应用中，服务端不存在 http://www.testurl.com/login 这样的地址会返回找不到该页面
- 在服务端应该除了静态资源外都返回单页应用的 index.html

### nginx配置history模式

```nginx
# conf/nginx.conf
location / {
root html;
index index.html index.htm;
#新添加内容
#尝试读取$uri(当前请求的路径)，如果读取不到读取$uri/这个文件夹下的首页
#如果都获取不到返回根目录中的 index.html
try_files $uri $uri/ /index.html;
}
```

## Vue-Router原理

### 类图

![router图示](vue-router-class.png)

- options 构造函数初始化参数，即路由规则
- routeMap 路由地址及组件的关系
- data 响应式对象，current属性记录当前路由
- install 静态方法，提供给vue use的插件机制使用

## Vue-Router实现

### 1. install 方法

```js
let _Vue = null;
export default class VueRouter {
    static install (Vue) {
        // 判断当前插件是否已经安装
        if（VueRouter.install.installed) {
            return
        }
        VueRouter.install.installed = true
        // 把vue构造函数记录到全局变量
        _Vue = Vue
        // 把创建vue实例时候传入的router对象注入到vue实例中
        _Vue.mixin({
            // 
            beforeCreate() {
                // vue组件不执行此方法
                if （this.$options.router） {
        			_Vue.prototype.$router =  this.$options.router
                    this.$options.router.init()
                }
            }
        })
    }
}
```

### 2.构造函数

```js
constructor(options) {
    this.options = options
    this.routeMap = {}
    this.data = _Vue.observable({
        current: '/'
    })
}
```

### 3.createRouteMap

```js
createRouteMap() {
    // 遍历路由规则，解析成键值对形式，放到routeMap
    this.options.routes.forEach(route => {
        this.routeMap[route.path] = route.component
    })
}
```

### 4. router-link

```js
initComponents(Vue) {
    Vue.component('router-link', {
        props: {
            to: String
        },
        template: '<a  :href="to"><slot></slot></a>'
    })
}

init() {
    this.createRouteMap();
    this.initComponents(_Vue);
    this.initEvent()
}
```

由于vue的模板需要编译，而vue的运行时版本不包含template模板，需要打包时提前编译，所以需要使用完整版(步骤5)，或者在运行时版本使用render函数(步骤6)

### 5. 使用完整vue

```js
// vue.config.js
module.export = {
    runtimeCompiler: true
}
```

### 6. 运行时版本的vue

```js
// 修改步骤4的函数
initComponents(Vue) {
    Vue.component('router-link', {
        props: {
            to: String
        },
        render(h) {
            return h('a', {
                attrs: {
                    href: this.to
                }
            }, [this.$slots.default])
        }
        // 运行时版本的vue不支持template
        // template: '<a  :href="to"><slot></slot></a>'
    })
}
```

### 7.router-view

```js
initComponents(Vue){
    Vue.component('router-link', {
        props: {
            to: String
        },
        render(h) {
            return h('a', {
                attrs: {
                    href: this.to
                },
                on: {
                    click:this.clickHandler
                }
            }, [this.$slots.default])
        },
        methods: {
            clickHandler(e) {
                history.pushState({}， '', this.to)
                this.$router.data.current = this.to
               e.preventDefault() 
            }           
        }
    })
    
    const self = this
    Vue.component('router-view',{
        render(h) {
           const component =  self.routeMap[self.data.current]
           return h(component)	
       }
    })
}
```

### 8.initEvent 处理浏览器前进后退

```js
initEvent() {
    window.addEventListener('popstate', () => {
        this.data.current = window.location.pathname
    })
}
```



# 模拟Vue响应式原理

## 数据响应式

修改数据时，视图会更新

### vue2

```js
// 通过Object.defineProperty 拦截属性的读取操作（ES5特性，ie8及以下版本不支持）
```

### vue3

通过proxy监听对象，而非属性。ES6新增，ie不支持。代码相比defineProperty简洁。性能由浏览器优化，相比defineProperty较好。

### 发布订阅模式

订阅者，发布者，信号中心

```js
class EventEmitter {
    constructor() {
        this.subs = Object.create(null)
    }
    $on(type, handler) {
        this.subs[type] = this.subs[type] || []
        this.subs[type].push(handler)
    }
    $emit(type, data) {
        if(this.subs[type]){
            this.subs[type].forEach(item => {
                item()
            })
        }
    }
}
```

### 观察者模式

```js
// 发布者-目标
class Dep{
    constructor() {
        this.subs = []
    }
    // 添加订阅者
    addSub(sub){
        if (sub && sub.update) {
            this.subs.push(sub)
        }
    }
    // 发布通知
    notify() {
        this.subs.forEach(sub => {
            sub.update()
        })
    }
}
// 订阅者-观察者
class Watcher{
    update() {
        // handler
    }
}

let dep = new Dep()
let watcher = new Watcher()
dep.addsub(watcher)
dep.notify()
```

### 观察者与发布订阅区别

![图示](difference.png)

### Vue类

![图示](vue-class.png)

实现

```js
class Vue{
    constructor(options) {
        // 保存选项数据
        this.$options = options || {}
        this.$data = options.data || {}
        this.$el = typeof options.el === 'string' ? document.querySelector(options.el) : options.el
        this._proxyData(this.$data)
        // 调用observer对象，监听数据变化
        new Observer(this.$data)
        // 调用compiler对象，解析指令和差值表达式
        new Compiler(this)
    }
    _proxyData(data) {
    	// 遍历data中的成员转换为getter和setter，注入到vue实例 
        Object.keys(data).forEach(key => {
            Object.defineProperty(this, key, {
                enumerable: true,
                configurable: true,
                get() {
                    return data[key]
                },
                set(newValue) {
                    if (newValue === data[key]) {
                        return
                    }
                    data[key] = newValue
                }
            })
        })
    }
}
```

### Observer

```js
class Observer {
    constructor(data) {
        this.walk(data)
    }
    walk(data) {
        if (!data || typeof data !== 'object') {
            return
        }
        Object.keys(data).forEach(key => {
            this.defineReactive(data, key, data[key])
        })
    }
    defineReactive(obj, key, val){
        const self = this
        // 把内部属性变成响应式
        this.walk(val)
        Object.defineProperty(obj, key, {
            enumerable: true,
            configurable: true,
            get() {
                return val
            }
            set(newValue) {
                if(newValue === val) {
                    return 
                }
            	val = newValue
            	// 如果赋值为对象，把里面的属性变为响应式
            	self.walk(newValue)
        	}
        })
    }
}
```

### Compiler

功能：

- 负责编译模板，解析指令/插值表达式
- 负责页面的首次渲染
- 数据变化后重新渲染视图

结构：

![图示](compiler.png)



# Virtual DOM实现原理

作用：

- 维护视图和状态的关系
- 复杂视图下提升渲染性能
- 可以渲染DOM之外的原生/SSR/小程序

## 模块

模块用于处理元素的属性/样式/事件等

### 常用模块

- attributes
  - 设置DOM元素的属性，使用setAttribute()
  - 处理布尔类型的值
- props
  - 设置DOM元素的属性 element[attr] = value
  - 不处理布尔类型的值
- class
  - 切换类样式，通过sel选择器
- dataset 设置data-*自定义属性
- eventlisteners 注册和移除事件
- style 设置行内样式，支持动画，delayed/remove/destroy

### 模块使用

步骤：

- 导入需要的模块
- init() 注册模块
  - 使用h函数创建VNode ，可以把第二个参数设置为对象，其他参数往后移
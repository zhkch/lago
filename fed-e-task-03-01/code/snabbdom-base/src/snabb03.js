import { h, init } from 'snabbdom'
// 1. 导入模块
import style from 'snabbdom/modules/style'
import eventlisteners from 'snabbdom/modules/eventlisteners'

// 注册模块
let patch = init([
    style,
    eventlisteners
])

// 3. 使用h函数的第二个参数传入模块需要的数据（对象）
let vnode = h('div', {
    style: {
        backgroundColor: 'red'
    },
    on: {
        click: eventHandler
    }
}, [
    h('h1', 'hello'),
    h('p', 'world')
])

function eventHandler() {
    console.log('click')
}

let app = document.querySelector('#app')
patch(app, vnode)
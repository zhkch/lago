import { h, thunk, init } from 'snabbdom'


let patch = init([])

let vnode = h('div#container', [
    h('h1', 'hello'),
    h('p', 'world')
])

let app = document.querySelector('#app')

let oldVnode = patch(app, vnode)

setTimeout(() => {
    vnode = h('div#container', [
        h('h1', 'hello snabb'),
        h('p', 'virtal DOM')
    ])
    patch(oldVnode, vnode)

    // 清空页面内容, 通过h函数生成空的注释节点
    patch(oldVnode, h('!'))
}, 2000);
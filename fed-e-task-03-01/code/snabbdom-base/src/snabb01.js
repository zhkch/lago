import { h, thunk, init } from 'snabbdom'
// console.log(h, thunk, init

// 参数： 模块数组
// 返回值： patch函数,对比两个vnode的差异更新到真是DOM
let patch = init([])
// 第一个参数： 标签+选择器
// 第二个参数：如果时字符串的化，就是标签中的内容
let vnode = h('div#container.cls', 'hello world')

let app = document.querySelector('#app')

// 第一个参数可以为DOM元素，内部会转换成VNode
// 第二个参数 VNode
// 返回值VNode
let oldVnode = patch(app, vnode)

//假设从服务器获取内容
vnode = h('div', 'content')
patch(oldVnode, vnode)

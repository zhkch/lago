class Observer {
    constructor(data) {
        this.walk(data)
    }
    walk(data) {
        if (!data || typeof data !== 'object') {
            return
        }
        Object.keys(data).forEach(key => {
            this.defineReactive(data, key, data[key])
        })
    }
    defineReactive(obj, key, val){
        // 把内部属性变成响应式
        const self = this
        // 收集依赖，发送通知
        let dep = new Dep()
        this.walk(val)
        Object.defineProperty(obj, key, {
            enumerable: true,
            configurable: true,
            get() {
                Dep.target && dep.addSub(Dep.target)
                return val
            },
            set(newValue) {
                if(newValue === val) {
                    return 
                }
                val = newValue
                // 如果赋值为对象，把里面的属性变为响应式
                self.walk(newValue)
                // 发送通知
                dep.notify()
        	}
        })
    }
}
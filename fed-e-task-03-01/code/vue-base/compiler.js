class Compiler{
    constructor(vm) {
        this.el = vm.$el
        this.vm = vm
        this.compile(this.el)
    }
    // 编译模板，处理文本节点和元素节点
    compile(el) {
        let childNodes = el.childNodes;
        Array.from(childNodes).forEach(node => {
            if (this.isTextNode(node)) {
                this.compileText(node)
            // }
            } else if (this.isElementNode(node)) {
                this.compileElement(node)
            }
            // 判断node节点，如有子节点，则要递归调用compile
            if (node.childNodes && node.childNodes.length) {
                this.compile(node)
            }
        })
    }
    // 编译元素节点，处理指令
    compileElement(node) {
        // 遍历所有属性节点
        Array.from(node.attributes).forEach(attr => {
            // 判断是否是指令
            let attrNamme = attr.name
            if (this.isDirective(attrNamme)) {
                attrNamme = attrNamme.substr(2)
                let key = attr.value
                //#region 处理v-on
                if (attrNamme.indexOf('on:') !== -1) {
                    let eventType = attrNamme.split(':')[1]
                    this.handleEvent(this, node, eventType, key)
                }
                //#endregion
                this.update(node, key, attrNamme)
            }
        })
    }

    handleEvent(vm, node, eventType, eventName) {
        // TODO: 这里仅处理直接调用实例方法，而没有传参
        node.addEventListener(eventType, () => {
            vm.vm.$options.methods[eventName]()
        })
    }

    update(node, key, attrName) {
        let updateFn = this[`${attrName}Updater`]
        updateFn && updateFn.call(this, node, this.vm[key], key)
    }
    // v-text
    textUpdater(node, value, key) {
        node.textContent = value
        new Watcher(this.vm, key, (newValue) => {
            node.textContent = newValue
        })
    }
    // v-model
    modelUpdater(node, value, key) {
        node.value = value
        new Watcher(this.vm, key, (newValue) => {
            node.value = newValue
        })
        // 双向绑定
        node.addEventListener('input', () => {
            this.vm[key] = node.value
        })
    }

    // 编译文本节点，处理差值表达式
    compileText(node) {
        let reg = /\{\{(.+?)\}\}/
        let value = node.textContent
        if (reg.test(value)) {
            let key = RegExp.$1.trim()
            node.textContent = value.replace(reg, this.vm[key])
            // 创建watch对象,数据改变时更新对象
            new Watcher(this.vm, key, (newValue) => {
                node.textContent = newValue
            })
        }
    }
    // 判断元素属性是否是指令
    isDirective(attrName){
        return attrName.startsWith('v-')
    }
    // 判断元素属性是否是文本节点
    isTextNode(node) {
        return node.nodeType === 3
    }
    // 判断节点是否是元素节点
    isElementNode (node) {
        return node.nodeType === 1
    }
}
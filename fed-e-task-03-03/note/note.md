# 状态管理

- state 驱动应用的数据源
- view 以声明方式将state映射到视图
- actions 响应在view上的用用户输入导致的状态变化

## 组件传值

- 父到子，通过props，子组件接收父组件的值
- 子到父，子组件emit事件，父组件通过v-on：来监听事件
- 不相关组件传值，使用自定义事件传递数据
- 通过ref获取子组件，调用子组件的方法

## VueX核心概念

专门为vue设计的状态管理库，采用集中式的方式存储需要共享的状态，解决复杂组件通信，数据共享。

使用情况：中大型单页面应用，多个视图依赖同一状态或者多个不同视图的行为需要变更同一状态

1. Store, Vuex使用单一状态树，所以每个应用仅仅包含一个store实例
2. state，状态数据，
3. getter，从state派生出一些状态，通过getter来处理。
4. mutation，更改state，同步操作。通过store.commit 调用
5. action，提交mutation，而不是直接变更状态，可以包含异步操作。通过store.dispatch调用
6. module，将stote对象分割成模块，每个模块用拥有自己的state，mutation，action，getter甚至嵌套子模块

## Vuex基本代码结构

```
// store.js
import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
export default new Vuex.Store({
    state: {},
    mutations: {},
    actions: {},
    modules: {}
})

// app.js
import store from './store'
new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
```

## 严格模式

在mutation之外修改state会报错，生产模式不要开启，会引起性能问题

## Vuex插件

- Vuex的插件就是一个函数
- 这个函数接收一个store的参数

```js
const myPlugin = store => {
    // store初始化后调用
    store.subscribe(mutation, state) => {
        // 每次mutation之后调用
        // mutations格式为{ type, payload}
    }
}

// 创建store前使用插件
const store = new Vuex.store({
    //...
    plugins: [myPlugin]
})
```



# SSR服务端渲染

**服务端渲染应用/同构应用：**

- 通过服务端渲染首屏直出，解决SPA应用首屏渲染慢以及不利于SEO的问题
- 通过客户端渲染接管页面内容交互得到更好的用户体验

**传统服务端渲染：**

- 客户端请求一个地址，服务端查询页面所需数据后，将数据与模板结合起来渲染为html，返回给客户端，客户端再做显示。
- 优点：数据结合模板生成动态页面
- 缺点：
  1. 前后端代码耦合在一起，不利于开发维护
  2. 前端没有足够的发挥空间
  3. 服务端压力大
  4. 用户体验一般

客户端渲染：

后端负责处理数据接口，前端负责将接口数据渲染到页面中

**同构渲染应用的问题**：

1. 开发条件有限

   - 浏览器特定的代码只能在某些声明周期钩子函数中使用
   - 一些外部扩展库可能需要特殊处理才能在服务端渲染应用中运行
   - 不能在服务端渲染期间操作DOM
   - 某些代码操作需要区分运行环境

2. 设计构建设置和部署的更多要求

   需要构建两个端，只能部署在node.js Server

3. 更多的服务器负载

   - 渲染完整的应用程序，需要大量占用CPU资源
   - 高流量环境下，需要准备服务器负载
   - 需要服务端渲染优化工作

# Nuxt

路由导航使用方法与vue-router基本一致，如果使用a标签会刷新页面

## 动态路由

目录结构：

```bash
pages/
--| _slug/
-----| comments.vue
-----| index.vue
--| users/
-----| _id.vue
--| index.vue
```

Nuxt.js生成的路由配置表如下：

```js
router: {
  routes: [
    {
      name: 'index',
      path: '/',
      component: 'pages/index.vue'
    },
    {
      name: 'users-id',
      path: '/users/:id?',
      component: 'pages/users/_id.vue'
    },
    {
      name: 'slug',
      path: '/:slug',
      component: 'pages/_slug/index.vue'
    },
    {
      name: 'slug-comments',
      path: '/:slug/comments',
      component: 'pages/_slug/comments.vue'
    }
  ]
}
```

## 嵌套路由

使用nuxt-child组件作为子路由出口，pages目录增加父路由同名的文件夹来存放子路由的文件

## 自定义路由

*Nuxt.js 默认的配置涵盖了大部分使用情形，可通过 nuxt.config.js 来覆盖默认的配置。*

通过router配置项来覆盖Nuxt.js默认的vue-router配置

例如：

```js
module.exports = {
  router: {
    base: '/app/'
  }
}
```

## 视图

![结构](construct.png)

### 模板

定制化默认的 html 模板，只需要在 src 文件夹下（默认是应用根目录）创建一个 `app.html` 的文件。

默认模板为：

```html
<!DOCTYPE html>
<html {{ HTML_ATTRS }}>
  <head {{ HEAD_ATTRS }}>
    {{ HEAD }}
  </head>
  <body {{ BODY_ATTRS }}>
    {{ APP }}
  </body>
</html>
```

### 布局

layouts目录新增组件，然后pages的页面通过layout配置项来指定当前页面所有布局。一旦在layouts目录新增名为defalut.vue的布局组件，所有pages的页面都会默认以default来作为默认的布局

## 异步数据

### asyncData方法

用法：

- 将asyncData返回的数据融合组件data方法返回数据一并给组件
- 调用时机：服务端渲染期间和客户端路由更新之间

注意事项：

- 只能在页面组件中使用
- 没有this，因为是在组件初始化之前被调用的

#### 上下文对象

asyncData方法被调用的时候，第一个参数被设定为当前页面的[上下文对象](https://zh.nuxtjs.org/api#%E4%B8%8A%E4%B8%8B%E6%96%87%E5%AF%B9%E8%B1%A1)



# 自动化部署

1. 生成github access token https://github.com/settings/tokens
2. 把值配置到项目设置中的secrets
3. 项目根目录创建 .github/workflows目录
4. 下载main.yml 到workflows目录
5. 修改配置
6. 配置pm2配置文件
7. 提交更新
8. 查看自动部署状态
9. 访问网站
10. 提交更新